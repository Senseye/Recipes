.PHONY: up app down

up: ; sudo docker-compose up -d
app: ; sudo docker exec -it go_recipes_app sh
down: ; sudo docker-compose down
