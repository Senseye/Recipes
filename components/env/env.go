package env

import "os"

func MongoDBURI() string {
	return os.Getenv("MONGODB_URI")
}
