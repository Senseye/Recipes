package searcher

import (
	"context"
	"gitlab.com/Senseye/Recipes/models/mongodb"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	melone = mongodb.Recipe{
		ID:   primitive.ObjectID([12]byte{1}),
		Name: "Melone pie",
		Composition: []mongodb.RecipeComposition{
			{
				Name:  "Melone",
				Value: 1,
			},
			{
				Name:  "Sugar",
				Value: 2,
			},
		},
	}

	meloneSalt = mongodb.Recipe{
		ID:   primitive.ObjectID([12]byte{2}),
		Name: "Melone pie",
		Composition: []mongodb.RecipeComposition{
			{
				Name:  "Melone",
				Value: 1,
			},
			{
				Name:  "Salt",
				Value: 1,
			},
		},
	}

	apple = mongodb.Recipe{
		ID:   primitive.ObjectID([12]byte{3}),
		Name: "Apple pie",
		Composition: []mongodb.RecipeComposition{
			{
				Name:  "Apple",
				Value: 1,
			},
			{
				Name:  "Sugar",
				Value: 2,
			},
		},
	}

	strawberry = mongodb.Recipe{
		ID:   primitive.ObjectID([12]byte{4}),
		Name: "Strawberry pie",
		Composition: []mongodb.RecipeComposition{
			{
				Name:  "Strawberry",
				Value: 1,
			},
			{
				Name:  "Sugar",
				Value: 1,
			},
			{
				Name:  "Milk",
				Value: 1,
			},
		},
	}
)

func fillFixtures(client *mongo.Client) error {
	collection := client.Database("recipes").Collection("recipes")

	err := collection.Drop(context.TODO())
	if err != nil {
		return err
	}

	_, err = collection.InsertMany(context.TODO(), []interface{}{
		melone,
		meloneSalt,
		apple,
		strawberry,
	}, nil)

	return err
}
