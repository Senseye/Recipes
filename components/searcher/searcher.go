package searcher

import (
	"context"
	"github.com/juju/errors"
	"gitlab.com/Senseye/Recipes/components/logger"
	"gitlab.com/Senseye/Recipes/models/mongodb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Searcher struct {
	client *mongo.Client
}

func NewSearcher(client *mongo.Client) *Searcher {
	return &Searcher{
		client: client,
	}
}

func (s *Searcher) SearchByName(name string) []mongodb.Recipe {
	return s.searchByFilter(bson.M{
		"name": name,
	})
}

func (s *Searcher) SearchByAllComposition(params []Param) []mongodb.Recipe {
	composition := make([]bson.M, len(params))

	// https://stackoverflow.com/questions/25014699/mongodb-multiple-elemmatch
	for i, param := range params {
		composition[i] = bson.M{
			"$elemMatch": bson.M{
				"name": param.Name,
				"value": bson.M{
					"$lte": param.Value,
				},
			},
		}
	}

	return s.searchByFilter(bson.M{
		"composition": bson.M{
			"$all": composition,
		},
	})
}

func (s *Searcher) SearchByAllCompositionNames(names []string) []mongodb.Recipe {
	return s.searchByFilter(bson.M{
		"composition.name": bson.M{
			"$all": names,
		},
	})
}

func (s *Searcher) SearchByAnyCompositionNames(names []string) []mongodb.Recipe {
	aggregateOptions := options.Aggregate()

	or := make([]bson.M, len(names))
	for i, name := range names {
		or[i] = bson.M{
			"$eq": []interface{}{"$$item.name", name},
		}
	}

	cursor, err := s.collection().Aggregate(context.TODO(), []bson.M{
		{
			"$match": bson.M{
				"composition.name": bson.M{
					"$in": names,
				},
			},
		},
		{
			"$addFields": bson.M{
				"match_composition": bson.M{
					"$filter": bson.M{
						"input": "$composition",
						"as":    "item",
						"cond": bson.M{
							"$or": or,
						},
					},
				},
			},
		},
		{
			"$addFields": bson.M{
				"match_composition_count": bson.M{"$size": "$match_composition"},
			},
		},
		{
			"$sort": bson.M{
				"match_composition_count": -1,
				"_id":                     1,
			},
		},
	}, aggregateOptions)

	if err != nil {
		logger.Error(errors.Trace(err))

		return nil
	}

	var result []mongodb.Recipe

	for cursor.Next(context.TODO()) {
		var receipe mongodb.Recipe
		err := cursor.Decode(&receipe)

		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		result = append(result, receipe)
	}

	return result
}

func (s *Searcher) searchByFilter(filter interface{}) []mongodb.Recipe {
	findOptions := options.Find()

	cursor, err := s.collection().Find(context.TODO(), filter, findOptions)

	if err != nil {
		logger.Error(errors.Trace(err))

		return nil
	}

	var result []mongodb.Recipe

	for cursor.Next(context.TODO()) {
		var receipe mongodb.Recipe
		err := cursor.Decode(&receipe)

		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		result = append(result, receipe)
	}

	return result
}

func (s *Searcher) collection() *mongo.Collection {
	return s.client.Database("recipes").Collection("recipes")
}
