package searcher

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Senseye/Recipes/components/env"
	"gitlab.com/Senseye/Recipes/components/logger"
	"gitlab.com/Senseye/Recipes/models/mongodb"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"testing"
)

func TestSearcher_SearchByName(t *testing.T) {
	client := getClient()

	assert.NoError(t, fillFixtures(client))

	searcher := NewSearcher(client)

	assert.Equal(t, 2, len(searcher.SearchByName("Melone pie")))
	assert.Equal(t, 1, len(searcher.SearchByName("Apple pie")))
	assert.Equal(t, 0, len(searcher.SearchByName("Tea pie")))
}

func TestSearcher_SearchByAllComposition(t *testing.T) {
	client := getClient()

	assert.NoError(t, fillFixtures(client))

	searcher := NewSearcher(client)

	assert.Equal(t, 2, len(searcher.SearchByAllComposition([]Param{
		{
			Name:  "Melone",
			Value: 1,
		},
	})))

	assert.Equal(t, 1, len(searcher.SearchByAllComposition([]Param{
		{
			Name:  "Melone",
			Value: 1,
		},
		{
			Name:  "Sugar",
			Value: 2,
		},
	})))

	assert.Equal(t, 1, len(searcher.SearchByAllComposition([]Param{
		{
			Name:  "Melone",
			Value: 2,
		},
		{
			Name:  "Sugar",
			Value: 3,
		},
	})))
}

func TestSearcher_SearchByAllCompositionNames(t *testing.T) {
	client := getClient()

	assert.NoError(t, fillFixtures(client))

	searcher := NewSearcher(client)

	assert.Equal(t, 3, len(searcher.SearchByAllCompositionNames([]string{"Sugar"})))
	assert.Equal(t, 2, len(searcher.SearchByAllCompositionNames([]string{"Melone"})))
	assert.Equal(t, 1, len(searcher.SearchByAllCompositionNames([]string{"Melone", "Sugar"})))
	assert.Equal(t, 1, len(searcher.SearchByAllCompositionNames([]string{"Apple"})))
	assert.Equal(t, 0, len(searcher.SearchByAllCompositionNames([]string{"Steal"})))
}

func TestSearcher_SearchByAnyCompositionNames(t *testing.T) {
	client := getClient()

	assert.NoError(t, fillFixtures(client))

	searcher := NewSearcher(client)

	assert.Equal(t, []mongodb.Recipe{
		apple,
		melone,
		strawberry,
	}, searcher.SearchByAnyCompositionNames([]string{"Apple", "Sugar"}))

	assert.Equal(t, 1, len(searcher.SearchByAnyCompositionNames([]string{"Apple"})))
	assert.Equal(t, []mongodb.Recipe(nil), searcher.SearchByAnyCompositionNames([]string{"Steal"}))
}

func getClient() *mongo.Client {
	client, err := mongo.NewClient(options.Client().ApplyURI(env.MongoDBURI()))

	if err != nil {
		logger.Critical(err)
	}

	err = client.Connect(context.TODO())

	if err != nil {
		logger.Critical(err)
	}

	return client
}
