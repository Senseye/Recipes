package mongodb

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type RecipeComposition struct {
	Name  string `bson:"name"`
	//Type  uint32 `bson:"type"`
	Value uint32 `bson:"value"`
}

type Recipe struct {
	ID          primitive.ObjectID  `bson:"_id,omitempty"`
	Name        string              `bson:"name"`
	Composition []RecipeComposition `bson:"composition"`
}
